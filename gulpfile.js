var gulp = require('gulp');
var replace = require('gulp-replace');
var argv = require('minimist')(process.argv.slice(2));

var sass = require('gulp-sass');
var postcss = require('gulp-postcss');
var autoprefixer = require('autoprefixer');

var webfonts = require('gulp-google-webfonts');

var rigger = require('gulp-rigger');
var uglify = require('gulp-uglify');

gulp.task('css', function () {

    return gulp.src('src/css/common.scss')
        .pipe(sass({
            includePaths: [
                'bower_components/foundation/scss/foundation/components'
            ],
            errLogToConsole: true
        }).on('error', function (error) {
            console.log(error.messageFormatted);
        }))
        .pipe(postcss([
            autoprefixer({browsers: ['> 0.01%']})
        ]))
        .pipe(gulp.dest('dist/css'));
});

gulp.task('font-awesome', function () {

    return gulp.src('bower_components/font-awesome/fonts/*.*')
        .pipe(gulp.dest('dist/fonts'));
});

gulp.task('webfonts', function () {

    return gulp.src('./fonts.list')
        .pipe(webfonts({}))
        .pipe(gulp.dest('dist/fonts'));
});

gulp.task('html', function () {

    return gulp.src('src/*.html')
        .pipe(gulp.dest('dist'));
});

gulp.task('js', function () {

    return gulp.src('src/js/entry.js')
        .pipe(rigger())
        //.pipe(uglify())
        .pipe(gulp.dest('dist/js'));
});

gulp.task('watch', function () {

    gulp.watch('src/css/**/*.scss', ['css']);

    gulp.watch('src/*.html', ['html']);

    gulp.watch('src/js/entry.js', ['js']);

    gulp.watch('bower_components/font-awesome/fonts/*.*', ['font-awesome']);
});

gulp.task('default', function (callback) {

    gulp.start('font-awesome', 'css', 'js', 'html', callback);
});